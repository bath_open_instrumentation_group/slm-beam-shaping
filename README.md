# SLM Beam Shaping

This repo contains the simulation work that I did to investigate whether you can turn Gaussian beams into flat-top ones using an SLM and conformal mapping.  It forms part of the SLM interference lithography work that we've been doing.

## Installing
This runs as a Python notebook.  I think it needs python 2 and nplab.  I should update it to python 3...